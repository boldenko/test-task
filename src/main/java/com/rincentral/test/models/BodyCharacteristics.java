package com.rincentral.test.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class BodyCharacteristics {
    private Integer body_length;
    private Integer body_width;
    private Integer body_height;
    private Set<String> body_style;
}
