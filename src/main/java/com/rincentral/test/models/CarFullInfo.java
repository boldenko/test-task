package com.rincentral.test.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CarFullInfo extends CarInfo {

    @JsonProperty("bodyCharacteristics")
    private BodyCharacteristics bodyCharacteristics;

    @JsonProperty("engineCharacteristics")
    private EngineCharacteristics engineCharacteristics;
}
