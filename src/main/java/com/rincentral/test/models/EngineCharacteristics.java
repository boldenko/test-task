package com.rincentral.test.models;

import com.rincentral.test.models.external.enums.EngineType;
import com.rincentral.test.models.external.enums.FuelType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EngineCharacteristics {
    private FuelType engine_type;
    private EngineType engine_cylinders;
    private Double engine_displacement;
    private Integer engine_horsepower;

}
