package com.rincentral.test.models.params;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarRequestParameters {
    private String country;
    private String segment;
    private Double minEngineDisplacement;
    private Integer minEngineHorsepower;
    private Integer minMaxSpeed;
    private String search;
    private Boolean isFull;
    private Integer year;
    private String bodyStyle;
}
