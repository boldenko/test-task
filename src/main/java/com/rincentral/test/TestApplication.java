package com.rincentral.test;

import com.rincentral.test.data.*;
import com.rincentral.test.models.CarFullInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}

	@Bean
	public CarRepository carRepo() {
		return new CarRepository();
	}

	@Bean
	public Enumerated<String> bodyStyles() {
		return new SetEnumerated<>();
	}

	@Bean
	public Enumerated<String> segments() {
		return new SetEnumerated<>();
	}

	@Bean
	public Enumerated<String> brands() {
		return new SetEnumerated<>();
	}

	@Bean
	public Enumerated<String> countries() {
		return new SetEnumerated<>();
	}
}
