package com.rincentral.test.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rincentral.test.data.CarRepository;
import com.rincentral.test.data.Enumerated;
import com.rincentral.test.models.BodyCharacteristics;
import com.rincentral.test.models.CarFullInfo;
import com.rincentral.test.models.CarInfo;
import com.rincentral.test.models.EngineCharacteristics;
import com.rincentral.test.models.external.ExternalBrand;
import com.rincentral.test.models.external.ExternalCar;
import com.rincentral.test.models.external.ExternalCarInfo;
import com.rincentral.test.models.external.enums.EngineType;
import com.rincentral.test.models.external.enums.FuelType;
import com.rincentral.test.models.external.enums.GearboxType;
import com.rincentral.test.models.external.enums.WheelDriveType;
import com.rincentral.test.models.params.CarRequestParameters;
import com.rincentral.test.models.params.MaxSpeedRequestParameters;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApiController {
    private final Logger logger = LoggerFactory.getLogger(ApiController.class);

    private final CarRepository carRepository;

    private final Enumerated<String> segments;
    private final Enumerated<String> brands;
    private final Enumerated<String> countries;
    private final Enumerated<String> bodyStyles;

    @PostConstruct
    public void boom() throws Exception {
        Map<Integer, ExternalBrand> oldBrands = new HashMap<>();

        RestTemplate template = new RestTemplate();
        ObjectMapper mapper = new ObjectMapper();

        String brandsRequest = template.getForObject("http://localhost:8084/api/v1/brands", String.class);
        List<ExternalBrand> listOfBrands = mapper.readValue(brandsRequest, new TypeReference<>() {});

        for (ExternalBrand b : listOfBrands)
            oldBrands.put(b.getId(), b);

        String carsRequest = template.getForObject("http://localhost:8084/api/v1/cars", String.class);
        List<ExternalCar> listOfCars = mapper.readValue(carsRequest, new TypeReference<>() {});

        List<Integer> car_ids = listOfCars.stream().map(e -> e.getId()).collect(Collectors.toList());

        // Скачиваем машины
        for (Integer id : car_ids) {

            String carFullInfo = template.getForObject("http://localhost:8084/api/v1/cars/"+id, String.class);
            ExternalCarInfo ecfi = mapper.readValue(carFullInfo, new TypeReference<>() {});

            CarFullInfo cfi = new CarFullInfo();
            cfi.setId(ecfi.getId());
            cfi.setSegment(segments.find(ecfi.getSegment()));
            ExternalBrand externalBrand = oldBrands.get(ecfi.getBrandId());
            cfi.setBrand(brands.find(externalBrand.getTitle()));
            cfi.setModel(ecfi.getModel());
            cfi.setCountry(countries.find(externalBrand.getCountry()));
            cfi.setGeneration(ecfi.getGeneration());
            cfi.setModification(ecfi.getModification());
            cfi.setMaxSpeed(ecfi.getMaxSpeed());
            cfi.setYearRange(ecfi.getYearsRange());

            cfi.setEngineCharacteristics(new EngineCharacteristics(ecfi.getFuelType(), ecfi.getEngineType(), ecfi.getEngineDisplacement().doubleValue(), ecfi.getHp()));
            Set<String> styles = Arrays.stream(ecfi.getBodyStyle().split(", ")).map(style -> bodyStyles.find(style.trim())).collect(Collectors.toSet());
            cfi.setBodyCharacteristics(new BodyCharacteristics(ecfi.getBodyLength(), ecfi.getBodyWidth(), ecfi.getBodyLength(), styles));

            carRepository.put(cfi);
        }

        logger.info("We good!");
    }


    private Map<String, BiPredicate<CarFullInfo, Object>> filters = new HashMap<>();
    {
        filters.put("country", (carFullInfo, comparing) -> carFullInfo.getCountry().equals(comparing));
        filters.put("segment", (carFullInfo, comparing) -> carFullInfo.getSegment().equals(comparing));
        filters.put("minEngineDisplacement", ((carFullInfo, comparing) -> carFullInfo.getEngineCharacteristics().getEngine_displacement().compareTo((Double) comparing) >= 0));
        filters.put("minEngineHorsepower", (carFullInfo, comparing) -> carFullInfo.getEngineCharacteristics().getEngine_horsepower().compareTo((Integer) comparing) >= 0);
        filters.put("minMaxSpeed", (carFullInfo, comparing) -> carFullInfo.getMaxSpeed().compareTo((Integer) comparing) >= 0);
        filters.put("search", (carFullInfo, comparing) -> carFullInfo.getModel().indexOf((String) comparing) != -1 ||
                carFullInfo.getGeneration().indexOf((String) comparing) != -1 ||
                carFullInfo.getModification().indexOf((String) comparing) != -1);

        filters.put("isFull", (carFullInfo, comparing) -> true);
        filters.put("year", (carFullInfo, comparing) -> Integer.valueOf(carFullInfo.getYearRange().substring(0,4)).compareTo((Integer) comparing) >= 0);
        filters.put("bodyStyle", (carFullInfo, comparing) -> carFullInfo.getBodyCharacteristics().getBody_style().contains(comparing));

    }

    @GetMapping("/cars")
    public ResponseEntity<List<? extends CarInfo>> getCars(CarRequestParameters requestParameters, BindingResult brs) {

        if (brs.hasErrors())
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.BAD_REQUEST);
        else {
            Map<String, Object> request = new ObjectMapper().convertValue(requestParameters, Map.class);

            Stream<CarFullInfo> carStream = carRepository.select((cfi) -> true).stream();

            for (String key : request.keySet())
                carStream = carStream.filter(v -> filters.get(key).test(v,request.get(key)));

            List<CarFullInfo> response = carStream.collect(Collectors.toList());

            if (requestParameters.getIsFull() != null && requestParameters.getIsFull() == true)
                return new ResponseEntity<>(response, HttpStatus.OK);
            else {
                List<CarInfo> skimpy = response.stream().map(car -> new CarInfo(car.getId(),
                        car.getSegment(), car.getBrand(), car.getModel(), car.getCountry(),
                        car.getGeneration(), car.getModification(), car.getMaxSpeed(), car.getYearRange()))
                        .collect(Collectors.toList());
                return new ResponseEntity<>(skimpy, HttpStatus.OK);
            }
        }
    }

    @GetMapping("/fuel-types")
    public ResponseEntity<List<String>> getFuelTypes() {
        return ResponseEntity.ok(Stream.of(FuelType.values()).map(FuelType::name).collect(Collectors.toList()));
    }

    @GetMapping("/body-styles")
    public ResponseEntity<List<String>> getBodyStyles() {
        return ResponseEntity.ok(bodyStyles.values());
    }

    @GetMapping("/engine-types")
    public ResponseEntity<List<String>> getEngineTypes() {
        return ResponseEntity.ok(Stream.of(EngineType.values()).map(EngineType::name).collect(Collectors.toList()));
    }

    @GetMapping("/wheel-drives")
    public ResponseEntity<List<String>> getWheelDrives() {
        return ResponseEntity.ok(Stream.of(WheelDriveType.values()).map(WheelDriveType::name).collect(Collectors.toList()));
    }

    @GetMapping("/gearboxes")
    public ResponseEntity<List<String>> getGearboxTypes() {
        return ResponseEntity.ok(Stream.of(GearboxType.values()).map(GearboxType::name).collect(Collectors.toList()));
    }

    @GetMapping("/max-speed")
    public ResponseEntity<Double> getMaxSpeed(MaxSpeedRequestParameters requestParameters) {
        if (requestParameters.getBrand() == null && requestParameters.getModel() == null)
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        if (requestParameters.getBrand() != null && requestParameters.getModel() != null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);

        List<CarFullInfo> select = carRepository.select(p -> true);

        OptionalDouble response;
        if (requestParameters.getBrand() != null) {
            response = select.stream().filter(car -> car.getBrand().equals(requestParameters.getBrand())).mapToDouble(CarFullInfo::getMaxSpeed).average();
        } else
            response = select.stream().filter(car -> car.getModel().equals(requestParameters.getModel())).mapToDouble(CarFullInfo::getMaxSpeed).average();

        if (response.isPresent())
            return ResponseEntity.ok(response.getAsDouble());
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
}
