package com.rincentral.test.data;

import com.rincentral.test.models.CarFullInfo;
import com.rincentral.test.models.CarInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CarRepository {
    private final Map<Integer, CarFullInfo> data = new HashMap<>();

    public void put(CarFullInfo cfi) {
        data.put(cfi.getId(), cfi);
    }

    public CarFullInfo get(Integer id) {
        return data.get(id);
    }

    public List<CarFullInfo> select(Predicate<CarFullInfo> p) {
        return data.values().stream().filter(value -> p.test(value)).collect(Collectors.toList());
    }
}
