package com.rincentral.test.data;

import java.util.List;

public interface Enumerated<T> {
    T find(T entity);
    List<T> values();
}
