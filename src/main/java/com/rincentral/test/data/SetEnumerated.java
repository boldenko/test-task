package com.rincentral.test.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetEnumerated<T> implements Enumerated<T> {
    private Set<T> values = new HashSet<>();

    @Override
    public T find(T entity) {
        if (!values.contains(entity))
            values.add(entity);

        for (T v : values)
            if (v.equals(entity))
                return v;

        return null;
    }

    @Override
    public List<T> values() {
        return new ArrayList<>(values);
    }
}
